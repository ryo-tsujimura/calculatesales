package jp.alhinc.tsujimura_ryo_calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CulculateSales {

	public static void main(String[] args) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();
		// branchNames=("支店コード(key)","支店名(value)"
		// branchSales=("支店コード(key)","売上(value)"
		// branchSales=("商品コード(key)","商品名(value)"
		// branchSales=("商品コード(key)","商品合計金額(value)"

		//System.out.println("ここにあるファイルを開きます => " + args[0]);
		// コマンドライン引数を指定(\で区切っているエクスプローラーの住所)

		//入力処理(リスト)
		if (!input(args[0], "branch.lst", branchNames, branchSales, "支店", "[0-9]{3}")) {
			return;
		}
		if (!input(args[0], "commodity.lst", commodityNames, commoditySales, "商品", "[A-Za-z0-9]{8}+$")) {
			return;
		}

		//コマンドラインのファイルをすべて読み込んで売上ファイルのファイルだけをリストに格納
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			files[i].getName(); //ファイルの名前を取得

			if (files[i].isFile() && files[i].getName().matches("[0-9]{8}.rcd")) {
				rcdFiles.add(files[i]); //該当ファイルであったらリストへ格納
			}
		}

		//売上ファイル名が連番になって抜けがないかを確認
		Collections.sort(rcdFiles); //読み込んだファイルを昇順にソート
		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			//				System.out.println(rcdFiles.get(i)); //確認用

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8)); //ファイル名を取得、文字をカウントし、int型へ変換
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//仕分けしてリスト(rcdFiles)に格納した売上ファイルを読み込み[支店コード]・[商品コード]・[売上]を一セットとしてリスト(fileInfo)に格納
		//格納した支店コードをbranchSalesのkeyとして用い、引き出したvalueと売り上げを足す
		//足した値でbranchSalesのvalueの値を上書きして次の繰り返しへ

		BufferedReader br = null;

		for (int i = 0; i < rcdFiles.size(); i++) {

			try {

				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				List<String> fileInfo = new ArrayList<>(); //[支店コード]・[商品コード]・[売上]の1セットで格納

				String line;
				while ((line = br.readLine()) != null) {
					fileInfo.add(line);

				}

				if (fileInfo.size() != 3) {
					System.out.println(rcdFiles.get(i) + "のフォーマットが不正です");
					return;
				}

				if (!branchNames.containsKey(fileInfo.get(0))) {
					System.out.println(rcdFiles.get(i) + "の支店コードが不正です");
					return;

				}

				if (!commodityNames.containsKey(fileInfo.get(1))) {
					System.out.println(rcdFiles.get(i) + "の商品コードが不正です");
					return;
				}

				if (!fileInfo.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long fileSale = Long.parseLong(fileInfo.get(2)); //String型の値をキャストして初期化

				Long saleAmount = branchSales.get(fileInfo.get(0)) + fileSale; //branchSalesのvalueと売り上げを足したもので初期化
				Long commoditySaleAmount = commoditySales.get(fileInfo.get(1)) + fileSale; //commoditySalesのvalueと売り上げを足したもので初期化

				if (saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				if (commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(fileInfo.get(0), saleAmount); //branchSalesのvalueを更新した売上金額で上書き
				commoditySales.put(fileInfo.get(1), commoditySaleAmount); //commoditySalesのvalueを更新した売上金額で上書き

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//出力処理
		if (!output(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
		if (!output(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}

	}

	//入力メソッド(リスト)
	// branch.lstのファイルを1行ずつ読み込み、branchNamesに支店コードと支店名で格納
	public static boolean input(String path, String fileName, Map<String, String> names, Map<String, Long> sales,
			String word, String regex) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);

			if (!file.exists()) {
				System.out.println(word + "定義ファイルが存在しません"); //支店定義ファイルがあるか確認
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");

				if (((items.length != 2) || (!items[0].matches(regex)))) {
					System.out.println(word + "定義ファイルのフォーマットが不正です"); //定義ファイルのフォーマットの確認
					return false;
				}

				names.put(items[0], items[1]);
				sales.put(items[0], 0L);

			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return false;
				}
			}
		}
		return true;

	}

	//出力メソッド
	public static boolean output(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		//Mapに格納されているkeyの数(keySetメソッド)だけkey・value・valueで書き込みを行う(繰り返し)
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//拡張for文 すべての要素を先頭から一つずつ取り出す時に使用
			for (String key : names.keySet()) {

				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();

			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;

	}

}
